
<?php

# Fill our vars and run on cli
# $ php -f db-connect-test.php

$dbname = 'infomation_schema';
$dbuser = 'root';
$dbpass = 'password';
$dbhost = ' mysql-db';

$link = mysqli_connect($dbhost, $dbuser, $dbpass) or die("Unable to Connect to '$dbhost'");
mysqli_select_db($link, $dbname) or die("Could not open the db '$dbname'");

$test_query = "SHOW TABLES FROM $dbname";
$result = mysqli_query($link, $test_query);

$tblCnt = 0;
while($tbl = mysqli_fetch_array($result)) {
  $tblCnt++;
  #echo $tbl[0]."<br />\n";
}

if (!$tblCnt) {
  echo "There are no tables<br />\n";
} else {
  echo "There are $tblCnt tables<br />\n";
}
// https://gist.github.com/magnetikonline/650e30e485c0f91f2f40

class DumpHTTPRequestToFile {
        public function execute($targetFile) {
                $data = sprintf(
                        "%s %s %s\n\nHTTP headers:\n",
                        $_SERVER['REQUEST_METHOD'],
                        $_SERVER['REQUEST_URI'],
                        $_SERVER['SERVER_PROTOCOL']
                );

                foreach ($this->getHeaderList() as $name => $value) {
                        $data .= $name . ': ' . $value . "\n";
                }

                $data .= "\nRequest body:\n";

                file_put_contents(
                        $targetFile,
                        $data . file_get_contents('php://input') . "\n"
                );

                echo("Done!\n\n");
        }

        private function getHeaderList() {
                $headerList = [];
                foreach ($_SERVER as $name => $value) {
                        if (preg_match('/^HTTP_/',$name)) {
                                // convert HTTP_HEADER_NAME to Header-Name
                                $name = strtr(substr($name,5),'_',' ');
                                $name = ucwords(strtolower($name));
                                $name = strtr($name,' ','-');

                                // add to list
                                $headerList[$name] = $value;
                        }
                }

                return $headerList;
        }
}
(new DumpHTTPRequestToFile)->execute('./dumprequest.txt');
?>
